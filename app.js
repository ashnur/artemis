var restify = require('restify'),
    env = process.env;


var server = restify.createServer({name: 'ARTEMIS'});

server.get('/health', function (req, res) {
    res.send(200);
});

server.get(/.*/, restify.serveStatic({
    'directory': './public',
    'default': 'index.html'
}));
server.listen(env.NODE_PORT || 3000, env.NODE_IP || 'localhost', function () {
    console.log('%s listening at %s', server.name, server.url);
});