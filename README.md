# ARTEMIS
ARTEMIS is a tool used by front-end developers to interview potential candidates.

## Getting Started
To get started with ARTEMIS, all you have to do is to checkout the repo and then either issue the following commands.

```sh
> npm install
> node app
ARTEMIS listening at http://127.0.0.1:3000
```

or just run a static asset server like ```http-server``` from inside the ```/public``` folder and point your browser to the right url.

## Tests
For unit testing, ARTEMIS uses karma and jasmine. I **welcome any and all** attempts to extand the code coverage with more and more usefull unit tests. You can run ```npm test``` to execute the tests, please make sure that all tests are passing before submitting a PR

## Contributing
I welcome contributions to ARTEMIS!

* Pull requests are always welcome.
* Bug reports and feature requests are also welcomed.

Please read the [contributing guide](contributing.md) to understand the guiding principles and processes, for documentation on how to add more tests please refer to the missing [Guide to creating tests](tests.md).

## Docs
Currently the code is not yet documented, I **welcome any and all** attempts to fix this and make it more readable or easier understanble.

## Contact
If you have an idea or would just like to talk about ARTEMIS please feel free to reach out to my via my epam email. My time is limited but i welcome any discussion and i will reply as soon as i can.
