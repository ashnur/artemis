/*global window, ARTEMIS */
(function (global, ARTEMIS) {
    "use strict";

    ARTEMIS.Observ = {
        _listeners: {},
        subscribe: function (event, listener, bind) {
            bind = bind || null;

            if (this._listeners.hasOwnProperty(event)) {
                this._listeners[event].push({
                    fn: listener,
                    bind: bind
                });
            } else {
                this._listeners[event] = [{
                    fn: listener,
                    bind: bind
                }]
            }
            return function () {
                this._listeners[event] = this._listeners[event].filter(function (l) { l.fn !== listener; });
            }.bind(this);
        },
        dispatch: function (action, params) {
            params = params || {};
            if (!this._listeners.hasOwnProperty(action)) {
                return this;
            }

            this._listeners[action].forEach(function (listener) {
                listener.fn.call(listener.bind, params);
            });
            return this;
        },
        clear: function (){
            this._listeners = {};
        }
    };

}(this, this.ARTEMIS || {}));