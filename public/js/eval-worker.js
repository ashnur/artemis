onmessage = function (event) {
    try {
        var testResult;
        testResult = eval(event.data);
        postMessage(
            JSON.stringify(
                {
                    type: 'result',
                    content: testResult
                }
            )
        );
    } catch (e) {
        postMessage(
            JSON.stringify(
                {
                    type: 'error',
                    content: e.message + (e.lineNumber ? " on line " + (e.lineNumber - 2) : "")
                }
            )
        )
    }
};

function consoleLog() {
    postMessage(
        JSON.stringify(
            {
                type: 'log',
                content: arguments
            }
        )
    );
}

console.log = consoleLog;