/* global ARTEMIS */
(function (global, ARTEMIS) {

    function Router() {
        global.addEventListener('hashchange', this.hashChange.bind(this));
    };

    Router.prototype = Object.assign({}, clone(ARTEMIS.Observ), {
        _routes: [],
        _re: new RegExp('(:[A-z0-9_]+)', 'gi'),
        hashChange: function () {
            var url = location.hash.slice(1) || '/';
            this.resolve(url);
        },
        resolve: function (path) {
            var dispatch = path;
            this._routes.forEach(function (route) {
                var params = {}, i = 1, value;

                if (value = route.re.exec(path)) {
                    for (i = 1; i < value.length; i += 1) {
                        params[route.keys[i - 1]] = value[i];
                    }
                    this.dispatch(route.path, params);
                }
            }, this);
            //dispatch in case route is found as is
            this.dispatch(path);
        },
        on: function (path, fn, bind) {
            var keys = [], key;

            while (key = this._re.exec(path)) {
                keys.push(key[0].substr(1));
            }

            if (keys.length) {
                this._routes.push({
                    re: new RegExp(path.replace(this._re, '([A-z0-9_]+)'), 'gi'),
                    keys: keys,
                    path: path
                });
            }
            this.subscribe(path, fn, bind);
            return this;
        },
        navigateTo: function (path) {
            global.location.href = global.location.href.replace(/#(.*)$/, '') + '#' + path;
            return this;
        },
        flush: function () {
            this._listeners = {};
            this._routes = [];
        },
        start: function () {
            this.hashChange();
        }
    })

    ARTEMIS.router = new Router();
}(this, this.ARTEMIS || {}));
