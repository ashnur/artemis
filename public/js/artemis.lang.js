/*global window, ARTEMIS, console */
(function (window, ARTEMIS) {
  'use strict';
  
  var lang = {
    'game.over.time.message': 'Unfortunatly time ran out, thanks for playing.',
    'game.over.time.title': '<div class="emoticon"><div>:(</div></div>',
    'game.over.done.message': 'Yey, you\'ve made it to the end!<br />Final Time: {finalTime}',
    'game.over.done.title': '<div class="emoticon"><div>:)</div></div>',
    'game.load.error': 'Error loading relevant tests from origin.',
    'game.load.error.title': 'Sorry,',

    'game.test.run': 'Testing input "{funcCall}"',
    'game.test.wrong': 'FAIL: Got <strong>{result}</strong> but expected <strong>{expected}</strong>.',
    'game.test.right': 'PASS: <strong>{result}</strong> is the right answer.',
    'game.test.error': 'TEST: Error {error}.',
    'game.test.abandon': 'TIMEOUT: Your code took too long to execute.',


    'game.log':'CONSOLE: {msg}'
  };
  
  ARTEMIS.lang = {
    get: function (key, options) {
      var ret = key;
      options = options || {};

      if (!lang[key]) {
        console.error('ARTEMIS:LANG:', 'KEY: ' + key + ' not found!');
      } else {
        ret = lang[key];
        Object.keys(options).map(function (optionKey) {
          ret = ret.replace('{' + optionKey + '}', options[optionKey]);
        });
      }
      
      return ret;
    }
  };
  
}(this, this.ARTEMIS || {}));