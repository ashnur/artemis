/*global window, ARTEMIS, ace */
(function (window, ARTEMIS, ace) {
  "use strict";

  ARTEMIS.editor = function (id) {
    var editor;
    
    if (!id) {
      throw new Error('Id expected for new Editor');
    }
    
    editor = ace.edit(id);

    editor.setTheme("ace/theme/textmate");
    editor.getSession().setTabSize(4);
    editor.getSession().setUseSoftTabs(true);
    editor.setShowPrintMargin(false);
    editor.setFontSize("24px");
    editor.getSession().setMode("ace/mode/javascript");

    this.implementation = editor;
    this.disable();
  };
  
  ARTEMIS.editor.prototype = {
    set: function (value) {
      this.implementation.getSession().setValue(value);
      this.implementation.focus();
      return this;
    },
    get: function () {
      return this.implementation.getSession().getValue();      
    },
    enable: function () {
      this.implementation.setReadOnly(false);
      this.implementation.focus();
      return this;
    },
    disable: function () {
      this.implementation.setReadOnly(true);
      return this;
    }
  };

}(this, this.ARTEMIS || {}, ace));