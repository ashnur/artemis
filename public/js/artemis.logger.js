/*global window, ARTEMIS, console */
(function (window, ARTEMIS) {
  "use strict";

  ARTEMIS.logger = function (consoleElement) {
    consoleElement = consoleElement || document.createElement('div');
    return {
      log: function (msg, type) {        
        if (Array.isArray(msg)) {
          switch (msg[0]) {
            case 'test.run':
              msg = ARTEMIS.lang.get('game.test.run', { funcCall: msg[1] });
              break;
            case 'test.wrong':
              msg = ARTEMIS.lang.get('game.test.wrong', { result: msg[1], expected: msg[2] });
              break;
            case 'test.right':
              msg = ARTEMIS.lang.get('game.test.right', { result: msg[1] });
              break;
            case 'test.error':
              msg = ARTEMIS.lang.get('game.test.error', { error: msg[1] });
              break;
            case 'test.log':
              msg = Object.keys(msg[1]).map(function(arg){
                return JSON.stringify(msg[1][arg]);
              }).join(',');
              msg = ARTEMIS.lang.get('game.log', { msg: msg })
              break;  
            case 'worker.abandon':
              msg = ARTEMIS.lang.get('game.test.abandon');
              break;
          }
        }
        this.render(msg, type);
      },
      render: function (msg, type) {
        var typs = ['error', 'log', 'info', 'accept', 'intro', 'outtro'];

        type = (type !== undefined ? type : 'log');
        consoleElement.innerHTML += '<div' + (typs.indexOf(type) >= 0 ? ' class="' + type + '"' : '') + '>' + msg + '</div>';
        consoleElement.scrollTop = consoleElement.scrollHeight;

      },
      clear: function () {
        consoleElement.innerHTML = '';
      }
    };
  }
}(this, this.ARTEMIS || {}));