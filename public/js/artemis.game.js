/*global ARTEMIS */
(function (global, ARTEMIS) {
  'use strict';

  var editor = new ARTEMIS.editor('editor'),
    clock = new ARTEMIS.clock(document.getElementById("game-time")),
    logger = ARTEMIS.logger(document.getElementById('console')),
    router = ARTEMIS.router,
    inPlay = false,
    testJSONtoLoad = 'test-1.json',
    tests,
    test = new ARTEMIS.Test(editor, logger);

  test.subscribe('test.resume', function (currentTest) {
    clock.start();
  });
  test.subscribe('test.done', endGame);
  test.subscribe('test.complete', function (params) {
    logger.log(params.outtro, 'outtro');
    editor.disable();
    inPlay = false;
  });

  test.subscribe('test.start', function (params) {
    logger.clear();
    logger.log(params.intro, 'intro');
    editor.set(params.initial).enable();
  });

  test.subscribe('test.worker.abandon', function () {
    logger.log('worker.abandon');
  });

  test.subscribe('test.log', function (params) {
    logger.log(['test.log', params.result], 'log');
  });

  test.subscribe('test.error', function (params) {
    logger.log(['test.error', params.error], 'error');
  });

  test.subscribe('test.wrong', function (params) {
    logger.log(['test.wrong', params.result, params.expected], 'error');
  });

  test.subscribe('test.right', function (params) {
    logger.log(['test.right', params.result], 'accept');
  });

  test.subscribe('test.run', function (params) {
    logger.log(['test.run', params.functionCall], 'info');
  });

  test.subscribe('test.run.failed', function () {
    logger.log('test.run.failed', 'error');
  });

  router.on('/game', function () {
    var xhr = new XMLHttpRequest();
    xhr.open('get', '/interview-tests/' + testJSONtoLoad);
    xhr.onreadystatechange = function () {
      if (this.readyState === 4) {
        if (this.status === 200) {
          tests = JSON.parse(this.responseText).tests;
          startGame();
        } else {
          document.getElementById('end-message').innerHTML = ARTEMIS.lang.get('game.load.error');
          document.getElementById('end-title').innerHTML = ARTEMIS.lang.get('game.load.error.title');
          showPanel('game-message');
        }
      }
    };
    xhr.send();
  });

  router.on('/', function () {
    clock.reset();
    inPlay = false;
    showPanel('game-intro');
  });

  router.on('/sec/maskify', function () {
    clock.reset();
    inPlay = false;
    testJSONtoLoad = 'sec-maskify-only.json';
    showPanel('game-intro');
  });

  router.on('/sec/consecutive', function () {
    clock.reset();
    inPlay = false;
    testJSONtoLoad = 'sec-consecutive-strings.json';
    showPanel('game-intro');
  })

  router.on('/end-screen', function () {
    clock.stop();
    inPlay = false;
    document.getElementById('end-message').innerHTML = ARTEMIS.lang.get('game.over.done.message', { finalTime: clock.value() });
    document.getElementById('end-title').innerHTML =  ARTEMIS.lang.get('game.over.done.title');
    showPanel('game-message');
  })

  document.getElementById('game-start').addEventListener('click', function (e) {
    router.navigateTo('/game');
  }, false);

  document.getElementById('run-tests').addEventListener('click', function (e) {
    if (inPlay) {
      clock.stop();
      test.runTests(editor.get());
    } else {
      test.nextTest();
      clock.start();
      inPlay = true;
    }
  }, false);

  function startGame() {
    test.start(tests);
    clock.start();
    inPlay = true;
    showPanel('game-editor');
  }

  function showPanel(id) {
    document.querySelector('.game-panel.on').classList.remove('on');
    document.getElementById(id).classList.add('on');
  }

  function endGame() {
    inPlay = false;
    clock.stop();
    editor.disable();
    showPanel('game-message');
    document.getElementById('end-message').innerHTML = ARTEMIS.lang.get('game.over.done.message', { finalTime: clock.value() });
    document.getElementById('end-title').innerHTML = ARTEMIS.lang.get('game.over.done.title');
  }

  router.start();

}(this, this.ARTEMIS || {}));