describe('ARTEMIS Router', function () {
    var router = ARTEMIS.router, noop = function () { };

    beforeEach(function () {
        router.flush();
    });

    afterEach(function () {
        router.navigateTo('/');
    })

    it('should be possible to register routes', function () {
        router.on('/test', noop);
        expect(router._listeners['/test']).toBeDefined();
    });

    it('should call route handler in correct context', function () {

        var obj = { foo: 13 };
        var spy = jasmine.createSpy();

        router.on('/test2', spy, obj).resolve('/test2');

        expect(spy).toHaveBeenCalled();
        var calls = spy.calls.mostRecent();
        expect(calls.object).toEqual(obj);
    });

    it('should navigate to a new page and call route handler', function (done) {
        var spy = jasmine.createSpy();

        spy.and.callFake(function () {
            expect(spy).toHaveBeenCalled();
            done();
        });

        router.on('/navigateTo', spy).navigateTo('/navigateTo');
    });

    it('should handle routes with named params', function (done) {
        var spy = jasmine.createSpy();

        spy.and.callFake(function(){
            expect(spy).toHaveBeenCalledWith({ id: "23232", foobar: "12121" });
            done();
        });

        router.on('/test/:id/:foobar/query', spy).navigateTo('/test/23232/12121/query');
        
    });
    
});